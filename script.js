$(document).ready(function(){
    $("#myNavbar").on("click","a", function (event) {
        event.preventDefault();
        var id  = $(this).attr('href'),
            top = $(id).offset().top;
        $('body,html').animate({scrollTop: top-50}, 1500);
    });
});

particlesJS("particles-js", {"particles":{"number":{"value":52,"density":{"enable":true,"value_area":394.57382081613633}},"color":{"value":"#f2e26d"},"shape":{"type":"star","stroke":{"width":1,"color":"#ffffff"},"polygon":{"nb_sides":5},"image":{"src":"img/github.svg","width":100,"height":100}},"opacity":{"value":1,"random":false,"anim":{"enable":false,"speed":1,"opacity_min":0.1,"sync":false}},"size":{"value":7.891476416322726,"random":true,"anim":{"enable":false,"speed":40,"size_min":0.1,"sync":false}},"line_linked":{"enable":true,"distance":94.69771699587272,"color":"#ffffff","opacity":0.9075197878771135,"width":1},"move":{"enable":true,"speed":3.206824121731046,"direction":"none","random":false,"straight":false,"out_mode":"out","bounce":false,"attract":{"enable":false,"rotateX":600,"rotateY":1200}}},"interactivity":{"detect_on":"canvas","events":{"onhover":{"enable":true,"mode":"repulse"},"onclick":{"enable":true,"mode":"push"},"resize":true},"modes":{"grab":{"distance":231.44200550588337,"line_linked":{"opacity":0.9957014838380653}},"bubble":{"distance":400,"size":101.50965153766813,"duration":2,"opacity":0.9988549711306545,"speed":3},"repulse":{"distance":48.72463273808071,"duration":0.4},"push":{"particles_nb":4},"remove":{"particles_nb":2}}},"retina_detect":true});

var slides = document.querySelectorAll('#slides .slide');
var currentSlide = 0;
var slideInterval = setInterval(nextSlide,10000);

function nextSlide() {
 slides[currentSlide].className = 'slide';
 currentSlide = (currentSlide+1)%slides.length;
 slides[currentSlide].className = 'slide showing';
}
$(function(){
    $(window).scroll(function() {
        if($(this).scrollTop() != 0) {
            $('#toTop').fadeIn();
        } else {
            $('#toTop').fadeOut();
        }
    });
    $('#toTop').click(function() {
        $('body,html').animate({scrollTop:0},1500);
    });
});

